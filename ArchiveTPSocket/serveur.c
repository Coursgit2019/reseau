/******************************************************************************/
/*			Application: Mastermind	              */
/******************************************************************************/
/*									      */
/*			 programme  SERVEUR 				      */
/*									      */
/******************************************************************************/
/*									      */
/*		Auteurs :   Joan Besante / Arthur Palma / Victor Allègre   */
/*		Date :  26/04/2021						      */
/*									      */
/******************************************************************************/

#include <stdio.h>
#include <curses.h>

#include <sys/signal.h>
#include <sys/wait.h>
#include <stdlib.h>

#include "fon.h" /* Primitives de la boite a outils */

#include "mastermind.c"

#define SERVICE_DEFAUT "1111"

void serveur_appli(char *service); /* programme serveur */
char *itoa(int number);
int uno_pow(int number, int exponent);
int str_len(char *str);

/******************************************************************************/
/*---------------- programme serveur ------------------------------*/

int main(int argc, char *argv[])
{
	char *service = SERVICE_DEFAUT; /* numero de service par defaut */

	/* Permet de passer un nombre de parametre variable a l'executable */
	switch (argc)
	{
	case 1:
		printf("defaut service = %s\n", service);
		break;
	case 2:
		service = argv[1];
		break;

	default:
		printf("Usage:serveur service (nom ou port) \n");
		exit(1);
	}

	/* service est le service (ou numero de port) auquel sera affecte
	ce serveur*/

	serveur_appli(service);
}

/*
	Convert the given number into an array of chars
	Example: if the number if 1233, the output array will be [ '1', '2', '3', '3', '\0' ]
*/
char *itoa(int number)
{
	int cp = number;
	int len = 1;

	// Count how many digits make up the number
	// Example: for the number 1233, len will be equal to 4
	while (cp >= 10)
	{
		len++;
		cp /= 10;
	}

	// Allocate an array of chars for the number (len + 1 is to keep a place for the final character '\0')
	char *str_number = malloc(len + 1);

	// Convert each number into its ASCII version
	for (int i = 0; i < len; i++)
	{
		str_number[len - i - 1] = (number % uno_pow(10, i + 1)) / uno_pow(10, i) + 48;
	}

	// The final '\0' to close the array of chars
	str_number[len] = 0;
	return str_number;
}

// Raise a number to the exponent, it is the power function
int uno_pow(int number, int exponent)
{
	if (exponent == 0)
		return 1;
	else
		return number * uno_pow(number, exponent - 1);
}

// Return the length of a string
int str_len(char *str)
{
	int len = 0;
	while (str[len] != 0)
		len++;

	return len;
}

/******************************************************************************/
void serveur_appli(char *service)

/* Procedure correspondant au traitemnt du serveur de votre application */

{
	// Create a socket
	int server_socket_id = h_socket(AF_INET, SOCK_STREAM);

	if (server_socket_id == -1)
	{
		fprintf(stderr, "Couldn't create server socket!\n");
		exit(-1);
	}

	struct sockaddr_in *p_socket_addr;

	adr_socket(service, NULL, SOCK_STREAM, &p_socket_addr);

	// Bind the address to the socket
	h_bind(server_socket_id, p_socket_addr);

	// Start listening for possible connections
	h_listen(server_socket_id, 5);

	struct sockaddr_in p_socket_client_addr;
	int client_socket_id;

	// Accept a pending connection
	client_socket_id = h_accept(server_socket_id, &p_socket_client_addr);

	if (client_socket_id != 0)
	{
		time_t t;
		srand((unsigned)time(&t));
		// We generate a combination (see mastermind.c for further details)
		char *combination = generate_combination();

		printf("Generated Combination: ");
		for (int i = 0; i < COMBINATION_SIZE; i++)
		{
			printf("%c", combination[i]);
		}
		printf("\n");

		char *buffer = malloc(100);
		buffer[0] = COMBINATION_SIZE;

		// Send the generic combination size to the client
		h_writes(client_socket_id, buffer, 1);

		char *str_max_attempts = itoa(MAX_ATTEMPTS);
		char *str_str_max_attempts_len = itoa(str_len(str_max_attempts));

		// Send the length of the string corresponding to the maximum number of attempts to the client
		h_writes(client_socket_id, str_str_max_attempts_len, 1);

		// Send the maximum number of attempts to the client
		h_writes(client_socket_id, str_max_attempts, str_len(str_max_attempts));

		int combination_found = 0;

		// While the client has not found the combination
		while (!combination_found)
		{
			char *guess = malloc(COMBINATION_SIZE);

			// We wait for the client's guess to arrive
			h_reads(client_socket_id, guess, COMBINATION_SIZE);

			// We analyze the client combination and determine how close he is to the actual combination
			int *feedback = guess_combination(guess, combination);
			buffer[0] = feedback[0]; // Number of perfect guesses (color + position both accurate)
			buffer[1] = feedback[1]; // Number of good guesses (only color is accurate)

			// We send feedback to the client
			h_writes(client_socket_id, buffer, 2);

			// If there are as much perfect guesses as the combination length, we conclude that the client has found the combination
			combination_found = (feedback[0] == COMBINATION_SIZE);
		}

		// We close both the server socket and the client socket
		h_close(server_socket_id);
		h_close(client_socket_id);
	}
}