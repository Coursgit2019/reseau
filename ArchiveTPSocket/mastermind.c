#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define COMBINATION_SIZE 5
#define COLOR_COUNT 8
#define MAX_ATTEMPTS 12

char *generate_combination();
char random_color(char *color_pool);
int color_equal(char color1, char color2);
int char_to_upper(char c);
int *guess_combination(char *guess, char *combination);

char MM_COLORS[COLOR_COUNT] = {'R', 'O', 'Y', 'G', 'B', 'A', 'I', 'V'};

char *generate_combination()
{
    char *combination = malloc(COMBINATION_SIZE);

    for (int i = 0; i < COMBINATION_SIZE; i++)
    {
        combination[i] = random_color(MM_COLORS);
    }

    return combination;
}

char random_color(char *color_pool)
{
    return color_pool[rand() % COLOR_COUNT];
}

int color_equal(char color1, char color2)
{
    color1 = char_to_upper(color1);
    color2 = char_to_upper(color2);

    return color1 == color2;
}

int char_to_upper(char c)
{
    if (c >= 97 && c <= 122)
    {
        c -= 32;
    }
    return c;
}

int *guess_combination(char *guess, char *combination)
{
    int *matching_positions = malloc(COMBINATION_SIZE);

    for (int i = 0; i < COMBINATION_SIZE; i++)
    {
        matching_positions[i] = 0;
    }

    // Loop over the guess
    for (int i = 0; i < COMBINATION_SIZE; i++)
    {
        // Loop over the combination
        for (int j = 0; j < COMBINATION_SIZE; j++)
        {
            if (color_equal(guess[i], combination[j]))
            {
                if (i == j)
                {
                    matching_positions[j] = 2;
                    break;
                }
                else
                {
                    if (matching_positions[j] == 0)
                    {
                        matching_positions[j] = 1;
                    }
                }
            }
        }
    }

    int *feedback = malloc(2);
    for (int i = 0; i < COMBINATION_SIZE; i++)
    {
        if (matching_positions[i] == 2)
            feedback[0]++;
        else if (matching_positions[i] == 1)
            feedback[1]++;
    }

    return feedback;
}