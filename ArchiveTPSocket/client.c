/******************************************************************************/
/*			Application: Mastermind					*/
/******************************************************************************/
/*									      */
/*			 programme  CLIENT				      */
/*									      */
/******************************************************************************/
/*									      */
/*		Auteurs : Joan Besante / Arthur Palma / Victor Allègre	*/
/*									      */
/******************************************************************************/

#include <stdio.h>
#include <curses.h> /* Primitives de gestion d'ecran */
#include <sys/signal.h>
#include <sys/wait.h>
#include <stdlib.h>

#include "fon.h" /* primitives de la boite a outils */

#define SERVICE_DEFAUT "1111"
#define SERVEUR_DEFAUT "127.0.0.1"

void client_appli(char *serveur, char *service);
int uno_pow(int number, int exponent);
int uno_atoi(char *str_number, int len);

/*****************************************************************************/
/*--------------- programme client -----------------------*/

int main(int argc, char *argv[])
{

	char *serveur = SERVEUR_DEFAUT; /* serveur par defaut */
	char *service = SERVICE_DEFAUT; /* numero de service par defaut (no de port) */

	/* Permet de passer un nombre de parametre variable a l'executable */
	switch (argc)
	{
	case 1: /* arguments par defaut */
		printf("serveur par defaut: %s\n", serveur);
		printf("service par defaut: %s\n", service);
		break;
	case 2: /* serveur renseigne  */
		serveur = argv[1];
		printf("service par defaut: %s\n", service);
		break;
	case 3: /* serveur, service renseignes */
		serveur = argv[1];
		service = argv[2];
		break;
	default:
		printf("Usage:client serveur(nom ou @IP)  service (nom ou port) \n");
		exit(1);
	}

	/* serveur est le nom (ou l'adresse IP) auquel le client va acceder */
	/* service le numero de port sur le serveur correspondant au  */
	/* service desire par le client */

	client_appli(serveur, service);
}

/*****************************************************************************/
void client_appli(char *serveur, char *service)

/* procedure correspondant au traitement du client de votre application */

{
	// Create a socket
	int socket_id = h_socket(AF_INET, SOCK_STREAM);

	if (socket_id == -1)
	{
		fprintf(stderr, "Couldn't create client socket!\n");
		exit(-1);
	}

	struct sockaddr_in *p_socket_addr;
	adr_socket(service, serveur, SOCK_STREAM, &p_socket_addr);

	// Try connecting to the server
	h_connect(socket_id, p_socket_addr);

	char *buffer = malloc(100);

	// Read the generic combination size sent by the server
	h_reads(socket_id, buffer, 1);

	int combination_len = buffer[0];

	// Read the length of the string corresponding to the maximum number of attempts sent by the server
	h_reads(socket_id, buffer, 1);
	int str_max_attempts_len = buffer[0];

	// Read the maximum number of attempts sent by the server
	h_reads(socket_id, buffer, str_max_attempts_len - 48);
	int max_attempts = uno_atoi(buffer, str_max_attempts_len - 48);

	int attempts = 0;
	int combination_found = 0;

	printf("Available colors: R, O, Y, G, B, A, I, V\n");

	// While the combination is not found and the remaining number of attempts is above 0
	while (!combination_found && attempts < max_attempts)
	{
		printf("\n");
		printf("Enter a combination (%d characters) : ", combination_len);

		// Allow the user to input a possible combination
		char *guess = malloc(combination_len);
		scanf("%s", guess);

		// Send the combination to the server
		h_writes(socket_id, guess, combination_len);

		// Read and interpret the feedback regarding the sent combination
		h_reads(socket_id, buffer, 2);
		int perfect_guesses = buffer[0];
		int good_guesses = buffer[1];

		// If we have the maximum possible amount of perfect guesses, assume we have won
		if (perfect_guesses == combination_len)
		{
			combination_found = 1;
			printf("Congratulations! The combination was indeed: ");
			for (int i = 0; i < combination_len; i++)
			{
				printf("%c", guess[i]);
			}
			printf("\n");

			continue;
		}

		// Display feedback to the user
		printf("Perfect Guesses: %d\n", perfect_guesses);
		printf("Good Guesses: %d\n", good_guesses);

		attempts++;
		printf("Remaining attempts: %d\n", max_attempts - attempts);
	}

	if (!combination_found) {
		printf("You failed to guess the combination!\n");
	}
}

/*
	Convert the given array of chars into a number
	Example: if the array is [ '1', '2', '3', '3', '\0' ], the output number will be 1233
*/
int uno_atoi(char *str_number, int len)
{
	int res = 0;
	for (int i = 0; i < len; i++)
	{
		res += (str_number[i] - 48) * uno_pow(10, len - i - 1);
	}
	return res;
}

// Raise a number to the exponent, it is the power function
int uno_pow(int number, int exponent)
{
	if (exponent == 0)
		return 1;
	else
		return number * uno_pow(number, exponent - 1);
}

/*****************************************************************************/
